<?php
session_start();
$_SESSION["status"]        = "";
$_SESSION["staff_no"]      = "";
$_SESSION["member_code"]   = "";
$_SESSION["member_name"]   = "";
$_SESSION["member_group"]  = "";
?>

<?php 
include_once("navbar_1.php");
?> 
<body>

<Script Type="text/javascript">
  <!--
    function setDefault(){
      document.form1.staff_no.focus();
      document.form1.staff_no.value += "";
    }
    function enterNext(){
      if( window.event.keyCode == 13 ){
        document.form1.password.focus();
        document.form1.password.value += "";
      }
    }
    function enter(){
      if( window.event.keyCode == 13 ){
        document.form1.submit();
      }
    }
  //-->
</Script>


<!-- /////added jumbotron -->
<div class="container">
  <div class="jumbotron">
    
    <body onLoad="setDefault()">
      <div id="container">
       <!-- <div id="header"><div class="logo"><img src="common/img/logo.gif" alt="" width="274" height="60" border="0" /></div> -->
      </div>
      <div id="main">
       <div class="main_top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table width="200" height="158" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td valign="top" class="login"><div class="number">
                  <form id="form1" name="form1" action="check_site.php" method="post">
                    ユーザーＩＤ<br />
                    <input name="staff_no" type="text" class="inputtext" onKeyPress="enterNext();" style="ime-mode:disabled"/>
                    <br />
                    パスワード<br />
                    <input name="password" type="password" class="inputtext" onKeyPress="enter();" style="ime-mode:disabled"/>

                    <input type="hidden" name="mode" value="login" />
                  </form>
                </div>
              </br>
              <button type="submit" name="login" onClick="document.form1.submit()" class="btn btn-primary">ログイン</button>
            </td>
          </tr>
        </table></td>
        <td width="494" valign="top">
          <!--  -->
        </table>
        <br />
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <col width="30">
          <col width="50">
          <script language="javascript" type="text/javascript" src="js/oshirase.php"></script>
        </table>
      </td>
    </tr>
  </table>
  
</div>


</div>
<!-- </body> -->

</div>
</div>


<div class="container"> 
  <div class="About">
   <h2>
   LDBについて</h2>
   <h3>研究施設・研究所に対する強力なデータベースマーケティングが実現できます。</h3>
   <div class="h2_text">
    <P>ラボラトリデータベース（LDB）とは、ライフサイエンスを中心とした研究施設・研究所を、弊社独自の調査により整備・コード付けしたものです。<br>低コストで、網羅性と鮮度に優れたデータを継続的にご利用いただくことが可能です。<br>
      研究施設・研究所を顧客とする企業様においては、LDBをCRM（カスタマリレーションシップマネジメント）における顧客マスタとして活用しています。<br>
    また、CRMの実現に必要な各種システムモジュールを提供しています。貴社の業務内容・特徴に合せて、カスタマイズして提供いたします</P></div>

  </div>
</div>


<div class="container">
  <div class="Number of LDB registrations">

   <h3>LDB登録件数</h3>
   <p>機関・組織件数：108,097件，ラボ件数（組織内訳）：67,929件，研究者数：116,108件，予算獲得者数（研究者内訳）：47,538件</p>

 </div>  
</div>

<?php 
include_once("footer_1.php");
?> 

</body>


</html>


