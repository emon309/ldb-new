<html>
   
   <head>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <meta charset="UTF-8">
   </head>
<body>
<footer >
	
	<div class="container-fluid padding bg-dark" style="bottom:0;">
		<div class="row text-center  text-white">
			<div class="col-md-4">
				<a href="#">運営会社</a>
				<a href="#">プライバシーポリシー</a>
				<hr class="light text-white">
				
			</div>
			<div class="col-md-4">
				<div class="light  text-white">	
					<h5>Copyright &copy 2007-2020 SAGBRAIN Corp. All rights reserved</h5>
				</div>
				
			</div>

			<div class="col-md-4">
				<div class="light  text-white">	
					<a href="http://www.sagbrain.com" target="_blank"><img src="footer_logo.gif" alt="SAGBRAIN" border="0"></a>
				</div>
				
			</div>
		
	</div>

</footer>
</body>
</html>