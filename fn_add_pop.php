<?php
	require_once "include/auth_utf.inc";
	require_once "include/parts_utf.inc";

	$company_no = $_SESSION["company_no"];
	$staff_no = $_SESSION["staff_no"];

	$yyyy = date("Y");
	$mm   = date("m");
	$dd   = date("d");

	$msg = "";

	$seq            = trim(Array_Check($_GET,"seq"));
	$kikan_bunrui   = trim(Array_Check($_GET,"kikan_bunrui"));
	$kikan_name     = trim(Array_Check($_GET,"kikan_name"));
	$kikan_code     = trim(Array_Check($_GET,"kikan_code"));
	$sosiki_name    = trim(Array_Check($_GET,"sosiki_name"));
	$sosiki_code    = trim(Array_Check($_GET,"sosiki_code"));
	$up_sosiki_code   = trim(Array_Check($_GET,"up_sosiki_code"));
	$labo_flg       = trim(Array_Check($_GET,"labo_flg"));
	if(!$labo_flg){
		$labo_flg = "0";
	}
	$kennkyuusya_name = trim(Array_Check($_GET,"kennkyuusya_name"));
	$kennkyuusya_kana = trim(Array_Check($_GET,"kennkyuusya_kana"));
	$kennkyuusya_code = trim(Array_Check($_GET,"kennkyuusya_code"));
	$kennkyuusya_sosiki_code = trim(Array_Check($_GET,"kennkyuusya_sosiki_code"));
	$kikan_sosiki = trim(Array_Check($_GET,"kikan_sosiki"));

	$pref = trim(Array_Check($_GET,"pref"));
	$addr = trim(Array_Check($_GET,"addr"));
	$city = trim(Array_Check($_GET,"city"));
	$post_code = trim(Array_Check($_GET,"post_code"));
	$url = trim(Array_Check($_GET,"url"));
	$tel = trim(Array_Check($_GET,"tel"));

	$sosiki_pref = trim(Array_Check($_GET,"sosiki_pref"));
	$sosiki_addr = trim(Array_Check($_GET,"sosiki_addr"));
	$sosiki_city = trim(Array_Check($_GET,"sosiki_city"));
	$sosiki_post_code = trim(Array_Check($_GET,"sosiki_post_code"));
	$sosiki_url = trim(Array_Check($_GET,"sosiki_url"));
	$sosiki_tel = trim(Array_Check($_GET,"sosiki_tel"));

	$voice = trim(Array_Check($_GET,"voice"));
	$yakusyoku = trim(Array_Check($_GET,"yakusyoku"));

	$mode = Array_Check($_GET,"mode");

	$conn = Get_Conn();

	$error_flg="0";

	// GETパラメータ
	$where = "";

	if($kikan_name && !$kikan_code){
		$sql  = "select kikan_code from mt_kikan";
		$sql .= " where kikan_name ='".$kikan_name."' ";
		$rs = pg_query($conn,$sql);
		if($rs) {
			$dat = pg_fetch_assoc($rs);
			$kikan_code = Array_Check($dat, "kikan_code"); 
		}
	}
	if(!$kikan_name && $kikan_code){
		$sql  = "select kikan_name from mt_kikan";
		$sql .= " where kikan_code ='".$kikan_code."' ";
		$rs = pg_query($conn,$sql);
		if($rs) {
			$dat = pg_fetch_assoc($rs);
			$kikan_name = Array_Check($dat, "kikan_name"); 
		}
	}
	if($kikan_code){
		$sql  = "select * from mt_kikan";
		$sql .= " where kikan_code ='".$kikan_code."' ";
		$rs = pg_query($conn,$sql);
		if($rs) {
			$dat = pg_fetch_assoc($rs);
			$kikan_bunrui = Array_Check($dat, "kikan_bunrui"); 
			$pref = Array_Check($dat,"pref");
			$addr = Array_Check($dat,"addr");
			$city = Array_Check($dat,"city");
			$post_code = Array_Check($dat,"post_code");
			$url = Array_Check($dat,"url");
			$tel = Array_Check($dat,"tel");
		}
	}
/*
	if($sosiki_name && !$sosiki_code){
		$sql  = "select sosiki_code from mt_sosiki";
		$sql .= " where sosiki_name ='".$sosiki_name."' ";
		$sql .= "   and kikan_code ='".$kikan_code."' ";
		if( $up_sosiki_code ){
			$sql .= " and up_sosiki_code ='".$up_sosiki_code."' ";
		}
		$rs = pg_query($conn,$sql);
		if($rs) {
			$dat = pg_fetch_assoc($rs);
			$sosiki_code = Array_Check($dat, "sosiki_code"); 
		}
	}
*/
	if(!$sosiki_name && $sosiki_code){
		$sql  = "select sosiki_name from mt_sosiki";
		$sql .= " where sosiki_code ='".$sosiki_code."' ";
		if( $up_sosiki_code ){
			$sql .= " and up_sosiki_code ='".$up_sosiki_code."' ";
		}
		$rs = pg_query($conn,$sql);
		if($rs) {
			$dat = pg_fetch_assoc($rs);
			$sosiki_name = Array_Check($dat, "sosiki_name"); 
		}
	}
	if($sosiki_code){
		$sql  = "select labo_flg, up_sosiki_code from mt_sosiki";
		$sql .= " where sosiki_code ='".$sosiki_code."' ";
		$rs = pg_query($conn,$sql);
		if($rs) {
			$dat = pg_fetch_assoc($rs);
			$labo_flg = Array_Check($dat, "labo_flg"); 
			$up_sosiki_code = Array_Check($dat, "up_sosiki_code"); 
		}
	}
	if($kennkyuusya_name && !$kennkyuusya_code){
		$sql  = "select k.kennkyuusya_code from mt_kennkyuusya k";
		$sql .= " where k.name ='".$kennkyuusya_name."' ";
		$sql .= "   and exists (";
		$sql .= " select s.kennkyuusya_sosiki_code from mt_kennkyuusya_sosiki s";
		$sql .= " where s.kennkyuusya_code = k.kennkyuusya_code and s.sosiki_code ='".$sosiki_code."') ";
		$rs = pg_query($conn,$sql);
		if($rs) {
			$dat = pg_fetch_assoc($rs);
			$kennkyuusya_code = Array_Check($dat, "kennkyuusya_code"); 
		}
	}
	if(!$kennkyuusya_name && $kennkyuusya_code){
		$sql  = "select k.name from mt_kennkyuusya k";
		$sql .= " where k.kennkyuusya_code ='".$kennkyuusya_code."' ";
		$rs = pg_query($conn,$sql);
		if($rs) {
			$dat = pg_fetch_assoc($rs);
			$kennkyuusya_name = Array_Check($dat, "name"); 
		}
	}
	if($kennkyuusya_code && $sosiki_code){
		$sql  = "select kennkyuusya_sosiki_code from mt_kennkyuusya_sosiki";
		$sql .= " where sosiki_code ='".$sosiki_code."' ";
		$sql .= "   and kennkyuusya_code ='".$kennkyuusya_code."' ";
		$rs = pg_query($conn,$sql);
		if($rs) {
			$dat = pg_fetch_assoc($rs);
			$kennkyuusya_sosiki_code = Array_Check($dat, "kennkyuusya_sosiki_code"); 
		}
	}

	if($mode == "save"){
// 入力チェック
		$cnt = 0;
		if(!$kikan_name && !$kikan_code){
			$msg = "alert('機関情報を入力してください。');";
		}
		if($kikan_name && $kikan_code){
			$sql  = "select count(*) as cnt from mt_kikan";
			$sql .= " where kikan_name ='".$kikan_name."' ";
			$sql .= "   and kikan_code ='".$kikan_code."' ";
			$rs = pg_query($conn,$sql);
			if($rs) {
				$dat = pg_fetch_assoc($rs);
				$cnt = Array_Check($dat, "cnt"); 
			}
			if(! $cnt){
				$msg = "alert('機関名と機関コードが不一致です。');";
			}
		}
		if(!$msg){
			if(!$kikan_name && $kikan_code){
				$sql  = "select kikan_name from mt_kikan";
				$sql .= " where kikan_code ='".$kikan_code."' ";
				$rs = pg_query($conn,$sql);
				if($rs) {
					$dat = pg_fetch_assoc($rs);
					$kikan_name = Array_Check($dat, "kikan_name"); 
				}
				if(! $kikan_name){
					$msg = "alert('機関コードが不正です。');";
				}
			}
		}
/*
		if(!$msg){
			if($sosiki_name && $sosiki_code){
				$sql  = "select count(*) as cnt from mt_sosiki";
				$sql .= " where sosiki_name ='".$sosiki_name."' ";
				$sql .= "   and sosiki_code ='".$sosiki_code."' ";
				$rs = pg_query($conn,$sql);
				if($rs) {
					$dat = pg_fetch_assoc($rs);
					$cnt = Array_Check($dat, "cnt"); 
				}
				if(! $cnt){
					$msg = "alert('組織名と組織コードが不一致です。');";
				}
			}
		}
*/
		if(!$msg){
			if(!$sosiki_name && $sosiki_code){
				$sql  = "select sosiki_name from mt_sosiki";
				$sql .= " where sosiki_code ='".$sosiki_code."' ";
				$rs = pg_query($conn,$sql);
				if($rs) {
					$dat = pg_fetch_assoc($rs);
					$sosiki_name = Array_Check($dat, "sosiki_name"); 
				}
				if(! $sosiki_name){
					$msg = "alert('組織コードが不正です。');";
				}
			}
		}
		if(!$msg){
			if($kennkyuusya_name && $kennkyuusya_code){
				$sql  = "select count(*) as cnt from mt_kennkyuusya";
				$sql .= " where name ='".$kennkyuusya_name."' ";
				$sql .= "   and kennkyuusya_code ='".$kennkyuusya_code."' ";
				$rs = pg_query($conn,$sql);
				if($rs) {
					$dat = pg_fetch_assoc($rs);
					$cnt = Array_Check($dat, "cnt"); 
				}
				if(! $cnt){
					$msg = "alert('研究者名と研究者コードが不一致です。');";
				}
			}
		}
		if(!$msg){
			if(!$kennkyuusya_name && $kennkyuusya_code){
				$sql  = "select name from mt_kennkyuusya";
				$sql .= " where kennkyuusya_code ='".$kennkyuusya_code."' ";
				$rs = pg_query($conn,$sql);
				if($rs) {
					$dat = pg_fetch_assoc($rs);
					$kennkyuusya_name = Array_Check($dat, "name"); 
				}
				if(! $kennkyuusya_name){
					$msg = "alert('研究者コードが不正です。');";
				}
			}
		}
		if(!$msg){
			if(!$kikan_code && $sosiki_code){
				$msg = "alert('機関コード・組織コードが不正です。');";
			}
		}
		if(!$msg){
			if($up_sosiki_code){
				$sql  = "select count(*) as cnt from mt_sosiki";
				$sql .= " where sosiki_code ='".$up_sosiki_code."' ";
				$rs = pg_query($conn,$sql);
				if($rs) {
					$dat = pg_fetch_assoc($rs);
					$cnt = Array_Check($dat, "cnt"); 
				}
				if(! $cnt){
					$msg = "alert('上位組織コードが不正です。');";
				}
			}
		}
		if(!$msg){
			if($kennkyuusya_sosiki_code){
				$msg = "alert('研究者組織コードが登録済みです。');";
			}
		}

		if(!$msg){
			$msg = "alert('登録する内容はありません。');";
			pg_query($conn, "BEGIN");
			if($kikan_name && !$kikan_code){
				$sql  = "select ki_seq from mt_company where company_no='".$_SESSION["company_no"]."'";
				$rset = pg_query($conn,$sql);
				if($rset) {
					if(pg_num_rows($rset) != 0) {
						$array = pg_fetch_assoc($rset);
						if ($array["ki_seq"]!="") {
							$kikan_code = (int)substr($array["ki_seq"],2,6) + 1;
							$kikan_code = $_SESSION["company_id"].$kikan_code;
						} else {
							$kikan_code = $_SESSION["company_id"]."100001";
						}
					} else {
						$kikan_code = $_SESSION["company_id"]."100001";
					}
					$sql = "UPDATE mt_company SET ki_seq = '".$kikan_code."' WHERE company_no='".$_SESSION["company_no"]."'";
					pg_query($conn,$sql);
				}
				$sql = "INSERT INTO mt_kikan (kikan_bunrui, kikan_code, kikan_name, kari_flg, utime, uname, del_flg,";
				$sql .= "pref, addr, city, post_code, url, tel";
				$sql .= ") VALUES(";
				$sql .= "'".$kikan_bunrui."','".$kikan_code."','".$kikan_name."','1',date_trunc('second', now()),'".$staff_no."','0',";
				$sql .= "'".$pref."','".$addr."','".$city."','".$post_code."','".$url."','".$tel."')";
				if( pg_query($conn, $sql) == FALSE ){
					$msg = "alert('登録に失敗しました。');";
				} else {
					if($voice){
						$sql = "INSERT INTO voice (code, message) values(";
						$sql .= "'".$kikan_code."','".$voice."')";
						pg_query($conn,$sql);
					}
					$msg = "alert('登録しました。');";
				}
			}
			if($sosiki_name && !$sosiki_code){
				$sql  = "select so_seq from mt_company where company_no='".$_SESSION["company_no"]."'";
				$rset = pg_query($conn,$sql);
				if($rset) {
					if(pg_num_rows($rset) != 0) {
						$array = pg_fetch_assoc($rset);
						if ($array["so_seq"]!="") {
							$sosiki_code = (int)substr($array["so_seq"],2,6) + 1;
							$sosiki_code = $_SESSION["company_id"].$sosiki_code;
						} else {
							$sosiki_code = $_SESSION["company_id"]."300001";
						}
					} else {
						$sosiki_code = $_SESSION["company_id"]."300001";
					}
					$sql = "UPDATE mt_company SET so_seq = '".$sosiki_code."' WHERE company_no='".$_SESSION["company_no"]."'";
					pg_query($conn,$sql);
				}
				$sql = "INSERT INTO mt_sosiki (sosiki_code, kikan_code, up_sosiki_code, sosiki_name, kari_flg, utime, uname, del_flg,";
				$sql .= "labo_flg, pref, addr, city, post_code, url, tel";
				$sql .= ") VALUES(";
				$sql .= "'".$sosiki_code."','".$kikan_code."','".$up_sosiki_code."','".$sosiki_name."','1',date_trunc('second', now()),'".$staff_no."','0',";
				$sql .= "'".$labo_flg."','".$sosiki_pref."','".$sosiki_addr."','".$sosiki_city."','".$sosiki_post_code."','".$sosiki_url."','".$sosiki_tel."')";
				if( pg_query($conn, $sql) == FALSE ){
					$msg = "alert('登録に失敗しました。');";
				} else {
					if($voice){
						$sql = "INSERT INTO voice (code, message) values(";
						$sql .= "'".$sosiki_code."','".$voice."')";
						pg_query($conn,$sql);
					}
					$msg = "alert('登録しました。');";
				}
			}
			if($kennkyuusya_name && !$kennkyuusya_code){
				$sql  = "select ke_seq from mt_company where company_no='".$_SESSION["company_no"]."'";
				$rset = pg_query($conn,$sql);
				if($rset) {
					if(pg_num_rows($rset) != 0) {
						$array = pg_fetch_assoc($rset);
						if ($array["ke_seq"]!="") {
							$kennkyuusya_code = (int)substr($array["ke_seq"],2,6) + 1;
							$kennkyuusya_code = $_SESSION["company_id"].$kennkyuusya_code;
						} else {
							$kennkyuusya_code = $_SESSION["company_id"]."500001";
						}
					} else {
						$kennkyuusya_code = $_SESSION["company_id"]."500001";
					}
					$sql = "UPDATE mt_company SET ke_seq = '".$kennkyuusya_code."' WHERE company_no='".$_SESSION["company_no"]."'";
					pg_query($conn,$sql);
				}
				$sql = "INSERT INTO mt_kennkyuusya (kennkyuusya_code, name, kana, kari_flg, utime, uname, del_flg) VALUES(";
				$sql .= "'".$kennkyuusya_code."','".$kennkyuusya_name."','".$kennkyuusya_kana."','1',date_trunc('second', now()),'".$staff_no."','0')";
				pg_query($conn,$sql);
				if($voice){
					$sql = "INSERT INTO voice (code, message) values(";
					$sql .= "'".$kennkyuusya_code."','".$voice."')";
					pg_query($conn,$sql);
				}
				$msg = "alert('登録しました。');";
			}
			if($sosiki_code && $kennkyuusya_code && !$kennkyuusya_sosiki_code){
				$sql  = "select ks_seq from mt_company where company_no='".$_SESSION["company_no"]."'";
				$rset = pg_query($conn,$sql);
				if($rset) {
					if(pg_num_rows($rset) != 0) {
						$array = pg_fetch_assoc($rset);
						if ($array["ks_seq"]!="") {
							$kennkyuusya_sosiki_code = (int)substr($array["ks_seq"],2,6) + 1;
							$kennkyuusya_sosiki_code = $_SESSION["company_id"].$kennkyuusya_sosiki_code;
						} else {
							$kennkyuusya_sosiki_code = $_SESSION["company_id"]."700001";
						}
					} else {
						$kennkyuusya_sosiki_code = $_SESSION["company_id"]."700001";
					}
					$sql = "UPDATE mt_company SET ks_seq = '".$kennkyuusya_sosiki_code."' WHERE company_no='".$_SESSION["company_no"]."'";
					pg_query($conn,$sql);
				}
				$sql = "INSERT INTO mt_kennkyuusya_sosiki (kennkyuusya_sosiki_code, sosiki_code, kennkyuusya_code, kari_flg, utime, uname, del_flg, yakusyoku) VALUES(";
				$sql .= "'".$kennkyuusya_sosiki_code."','".$sosiki_code."','".$kennkyuusya_code."','1',date_trunc('second', now()),'".$staff_no."','0', '".$yakusyoku."')";
				pg_query($conn,$sql);
				if( pg_query($conn, $sql) == FALSE ){
					$msg = "alert('登録に失敗しました。');";
				} else {
					$msg = "alert('登録しました。');";
				}
			}
			pg_query($conn, "COMMIT");
		}
	} else {
		$kikan_sosiki = "1";
	}

	$style = "INPUTSTYLE1_1";
	$styleW = "INPUTSTYLE1_3";
	$readonly = "";
?>
<html>
<script type="text/javascript">
	<?=$msg?>
</script>
<body>
</body>
</html>
