<?php
	require_once "include/auth_utf.inc";
	require_once "include/parts_utf.inc";

	$kengen = $_SESSION["kengen"];

	$error_flg="0";

	// GETパラメータ
	$code = Array_Check($_GET,"code");
	$mode = Array_Check($_GET,"mode");

	$conn = Get_Conn();
	$msg = "";

	if( $mode == "1" ){
		$msg = "機関[${code}]を削除しました。";
		$sql = "UPDATE mt_kikan SET del_flg = '1' WHERE (kari_flg = '1' OR kari_flg = '2') AND kikan_code = '${code}'";
		if(! pg_query($conn,$sql)) {
			$msg = "<font color='red'>エラーが発生しました。</font>";
		}
	} else if( $mode == "3" ){
		$msg = "組織[${code}]を削除しました。";
		$sql = "UPDATE mt_sosiki SET del_flg = '1' WHERE (kari_flg = '1' OR kari_flg = '2') AND sosiki_code = '${code}'";
		if(! pg_query($conn,$sql)) {
			$msg = "<font color='red'>エラーが発生しました。</font>";
		}
	} else if( $mode == "5" ){
		$msg = "研究者[${code}]を削除しました。";
		$sql = "UPDATE mt_kennkyuusya SET del_flg = '1' WHERE (kari_flg = '1' OR kari_flg = '2') AND kennkyuusya_code = '${code}'";
		if(! pg_query($conn,$sql)) {
			$msg = "<font color='red'>エラーが発生しました。</font>";
		}
	} else if( $mode == "7" ){
		$msg = "研究者組織[${code}]を削除しました。";
		$sql = "UPDATE mt_kennkyuusya_sosiki SET del_flg = '1' WHERE (kari_flg = '1' OR kari_flg = '2') AND kennkyuusya_sosiki_code = '${code}'";
		if(! pg_query($conn,$sql)) {
			$msg = "<font color='red'>エラーが発生しました。</font>";
		}
		$sql = "UPDATE mt_kennkyuusya SET del_flg = '1' WHERE (kari_flg = '1' OR kari_flg = '2') AND kennkyuusya_code in ( SELECT kennkyuusya_code FROM mt_kennkyuusya_sosiki WHERE kennkyuusya_sosiki_code = '${code}' )";
		if(! pg_query($conn,$sql)) {
			$msg = "<font color='red'>エラーが発生しました。</font>";
		}
	} 
?>
<html>
<body>
<script>
if( "<?=$msg?>" ){
	alert("<?=$msg?>");
}
</script>
</body>
</html>
