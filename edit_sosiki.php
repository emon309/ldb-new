<?php
	require_once "include/auth_utf.inc";

	// GETパラメータ
	$sosiki_code  = Array_Check($_GET ,"sosiki_code");

	// POSTパラメータ
	$kikan_bunrui        = Array_Check($_POST,"kikan_bunrui");
	$kikan_code          = Array_Check($_POST ,"kikan_code");
	$up_sosiki_code      = Array_Check($_POST,"up_sosiki_code");
	$up_sosiki_name      = Array_Check($_POST,"up_sosiki_name");
	$sosiki_name         = Array_Check($_POST,"sosiki_name");
	$sosiki_kana         = Array_Check($_POST,"sosiki_kana");
	$sosiki_name_tuusyou = Array_Check($_POST,"sosiki_name_tuusyou");
	$labo_flg            = Array_Check($_POST,"labo_flg");
	$url                 = Array_Check($_POST,"url");
	$email               = Array_Check($_POST,"email");
	$pref                = Array_Check($_POST,"pref");
	$addr                = Array_Check($_POST,"addr");
	$city                = Array_Check($_POST,"city");
	$building            = Array_Check($_POST,"building");
	$post_code           = Array_Check($_POST,"post_code");
	$tel                 = Array_Check($_POST,"tel");
	$fax                 = Array_Check($_POST,"fax");
	$pref_code           = Array_Check($_POST,"pref_code");
	$area_code           = Array_Check($_POST,"area_code");
	$old_sosiki_code     = Array_Check($_POST,"old_sosiki_code");
	$old_sosiki_name     = Array_Check($_POST,"old_sosiki_name");
	$kari_flg            = Array_Check($_POST,"kari_flg");
	$del_flg             = Array_Check($_POST,"del_flg");


	$regist_staff_no    = Array_Check($_SESSION,"staff_no");

	$message = "";

	$conn = Get_Conn();

	pg_query($conn,"BEGIN");

	$sql_array = array();

	$sql_array["kikan_code"]      = "'".pg_escape_string($kikan_code)."'";
	$sql_array["up_sosiki_code"]  = "'".pg_escape_string($up_sosiki_code)."'";
	$sql_array["sosiki_name"]     = "'".pg_escape_string($sosiki_name)."'";
	$sql_array["sosiki_kana"]     = "'".pg_escape_string($sosiki_kana)."'";
	$sql_array["sosiki_name_tuusyou"] = "'".pg_escape_string($sosiki_name_tuusyou)."'";
	$sql_array["labo_flg"]        = "'".pg_escape_string($labo_flg)."'";
	$sql_array["url"]             = "'".pg_escape_string($url)."'";
	$sql_array["email"]           = "'".pg_escape_string($email)."'";
	$sql_array["pref"]            = "'".pg_escape_string($pref)."'";
	$sql_array["addr"]            = "'".pg_escape_string($addr)."'";
	$sql_array["city"]            = "'".pg_escape_string($city)."'";
	$sql_array["building"]        = "'".pg_escape_string($building)."'";
	$sql_array["post_code"]       = "'".pg_escape_string($post_code)."'";
	$sql_array["tel"]             = "'".pg_escape_string($tel)."'";
	$sql_array["fax"]             = "'".pg_escape_string($fax)."'";
	$sql_array["pref_code"]       = "'".pg_escape_string($pref_code)."'";
	$sql_array["area_code"]       = "'".pg_escape_string($area_code)."'";
	$sql_array["old_sosiki_code"] = "'".pg_escape_string($old_sosiki_code)."'";
	$sql_array["old_sosiki_name"] = "'".pg_escape_string($old_sosiki_name)."'";
	$sql_array["kari_flg"]        = "'".pg_escape_string($kari_flg)."'";
	$sql_array["del_flg"]         = "'".pg_escape_string($del_flg)."'";

	$sql_array["utime"] = "now()";

	if(strcmp($kikan_code,"") == 0) { // 新規登録時

		// 新規登録の場合は研究内容コードを取得
		$kikan_code = "";

		$sql  = "select max(sosiki_code) as max_code from mt_sosiki where sosiki_code > '30000000' and kikan_code < '40000000'";
		$rset = pg_query($conn,$sql);

		if($rset) {
			if(pg_num_rows($rset) != 0) {
				$array = pg_fetch_assoc($rset);

				$kikan_code = (int)$array["max_code"] + 1;
			} else {
				//$kikan_code = "'1'";
				$message = "機関コード取得時にエラーが発生しました";
			}
		}

		$sql_array["sosiki_code"] = "'".pg_escape_string($kikan_code)."'";

		$sql = Create_INSERT_SQL("mt_sosiki",$sql_array);
error_log($sql);

		if(pg_query($conn,$sql)) {
/*
			$sql_array["create_date"]          = "now()";
			$sql_array["update_date"]          = "now()";
			$sql_array["regist_staff_code"]    = "'".pg_escape_string($regist_staff_code)."'";
			$sql_array["regist_staff_comment"] = "'".pg_escape_string($regist_staff_comment)."'";
			$sql_array["query_type"]           = "'INSERT'";

			$sql = Create_INSERT_SQL("area_log",$sql_array);

			if(pg_query($conn,$sql)) {
				//
			} else {
				$message = "ログ登録時にエラーが発生しました";
			}
*/
		} else {
			$message = "登録時にエラーが発生しました";
		}
	} else { // 更新時

		$sql  = Create_UPDATE_SQL("mt_sosiki",$sql_array);
		$sql .= "where sosiki_code = '".pg_escape_string($sosiki_code)."'";
error_log($sql);
		if(pg_query($conn,$sql)) {
/*
			$sql_array["area_id"]              = pg_escape_string($area_id);
			$sql_array["area_code"]            = "'".pg_escape_string($area_code)."'";
			$sql_array["regist_staff_code"]    = "'".pg_escape_string($regist_staff_code)."'";
			$sql_array["regist_staff_comment"] = "'".pg_escape_string($regist_staff_comment)."'";
			$sql_array["query_type"]           = "'UPDATE'";

			$sql = Create_INSERT_SQL("area_log",$sql_array);

			if(pg_query($conn,$sql)) {
				//
			} else {
				$message = "ログ登録時にエラーが発生しました";
			}
*/
		} else {
			$message = "登録時にエラーが発生しました";
		}
	}

	if(strcmp($message,"") == 0) {
		$message = "登録が完了しました";
		pg_query($conn,"COMMIT");
	} else {
		// エラーメッセージが存在していた場合、データを元に戻す
		pg_query($conn,"ROLLBACK");
	}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift-JIS">
<link rel="stylesheet" href="/main.css" type="text/css" media="all" />
<title>マスター管理</title>
<script type="text/javascript">
<!--
	alert('<?=$message?>');

//	parent.window.location.reload();
//-->
</script>
</head>
<body>
</body>
</html>
