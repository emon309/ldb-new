var request = null;

function CreateHttpRequest() {
	try {
		request = new XMLHttpRequest();
	} catch(e) {
		try {
			request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch(e) {
			request = new ActiveXObject("Msxml2.XMLHTTP");
		}
	}
}

function SendRequest(method,url,value,exec_function) {
	var async = true;

	if(request == null) {
		CreateHttpRequest();
	}

	if(arguments.length >= 5) {
		async = arguments[4];
	}

	if(request.readyState != 0) { request.abort(); }

	request.onreadystatechange = exec_function;

	request.open(method,url,async);
	if(method.toLowerCase() == "post") {
		request.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	}
	request.send(value);
}

function GetResponseText() {
	if(request.readyState == 4) {
		if(request.status == 200) {
			return request.responseText;
		} else {
			return false;
		}
	}

	return false;
}

function GetResponseXML() {
	if(request.readyState == 4) {
		if(request.status == 200) {
			return request.responseXML;
		} else {
			return false;
		}
	}

	return false;
}

function URLEncode(str) {
	str = str.replace(/&/g ,'%26');
	str = str.replace(/#/g ,'%23');
	str = str.replace(/\+/g,'%28');
	str = str.replace(/ /g ,'%20');
	str = str.replace(/=/g ,'%3D');
	str = str.replace(/;/g ,'%3B');

	return str;
}

function InputLock(bool) {
	var obj = document.getElementsByTagName("input");

	for(var i = 0; i < obj.length; i++) {
		obj[i].disabled = bool ? "disabled" : "" ;
	}

	var obj = document.getElementsByTagName("select");

	for(var i = 0; i < obj.length; i++) {
		obj[i].disabled = bool ? "disabled" : "" ;
	}
}
