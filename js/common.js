var move = false;
var popup;
var offsety;
var offsetx;

function onMouse(bool) {
	move = bool;

	if(move) {
		offsety = event.offsetY;
		offsetx = event.offsetX;
		document.getElementById("edit_ifrm").style.display = "none";
	} else {
		document.getElementById("edit_ifrm").style.display = "block";
	}
}

function Move_Window() {
	if((event.button & 1) == 0) {
		onMouse(false);
	}

	if(move) {
		move_top  = event.clientY + document.body.scrollTop  - offsety;
		move_left = event.clientX + document.body.scrollLeft - offsetx;

		if(move_top  < 0) move_top  = 0;
		if(move_left < 0) move_left = 0;

		document.getElementById("popup").style.top  = move_top  + "px";
		document.getElementById("popup").style.left = move_left + "px";
	} else {
		//
	}
}

function Open_Window(url,width,height) {
	if(popup != null) {
		Close_Window();
	}

	width  = parseInt(width.replace("px",""));
	height = parseInt(height.replace("px",""));

	popup = document.createElement("div");
	popup.setAttribute("id","popup");
	popup.style.width    = (width  + 8);
	popup.style.height   = (height + 34);
	popup.style.display  = "block";
	popup.style.position = "absolute";
	popup.style.top      = (document.body.scrollTop  + 10) + "px";
	popup.style.left     = (document.body.scrollLeft + 10) + "px";

	popup_table  = "";
	popup_table += "<table cellpadding='0' cellspacing='0' border='0' style='background-color:#FFFFFF;'>";
	popup_table += "	<col width='4px'>";
	popup_table += "	<col width='" + (width - 25) + "'>";
	popup_table += "	<col width='25px'>";
	popup_table += "	<col width='4px'>";
	popup_table += "	<tr>";
	popup_table += "		<td style='height:30px;background-image:url(/img/popup/tl.gif);'></td>";
	popup_table += "		<td style='height:30px;background-image:url(/img/popup/tb.gif);' onmousedown='onMouse(true)' onmouseup='onMouse(false)'></td>";
	popup_table += "		<td style='height:30px;background-image:url(/img/popup/tb.gif);' valign='bottom'>";
	popup_table += "			<input type='image' src='/img/popup/bu.gif' onmousedown='this.src=\"/img/popup/bd.gif\"' onmouseout='this.src=\"/img/popup/bu.gif\"' onclick='Close_Window()'/>";
	popup_table += "		</td>";
	popup_table += "		<td style='height:30px;background-image:url(/img/popup/tr.gif);'></td>";
	popup_table += "	</tr>";
	popup_table += "	<tr>";
	popup_table += "		<td style='background-image:url(/img/popup/lb.gif);'></td>";
	popup_table += "		<td colspan='2' width='" + width + "' height='" + height + "'>";
	popup_table += "			<iframe id='edit_ifrm' name='ifrm' src='" + url + "' frameborder='0' scrolling='auto' width='" + width + "' height='" + height + "'></iframe>";
	popup_table += "		</td>";
	popup_table += "		<td style='background-image:url(/img/popup/rb.gif);'></td>";
	popup_table += "	</tr>";
	popup_table += "	<tr>";
	popup_table += "		<td style='height:4px;background-image:url(/img/popup/bl.gif);'></td>";
	popup_table += "		<td style='height:4px;background-image:url(/img/popup/bb.gif);' colspan='2'></td>";
	popup_table += "		<td style='height:4px;background-image:url(/img/popup/br.gif);'></td>";
	popup_table += "	</tr>";
	popup_table += "</table>";

	popup.innerHTML = popup_table;

	document.body.appendChild(popup);
	document.body.onmousemove = function () { Move_Window(); };
}

function Close_Window() {
	document.body.onmousemove = null;
	document.body.removeChild(popup);
	popup = null;
}

function view_list_title(title, tag, img, sz){
	if( tag.style.display == "none" ){
		title.style.display = "";
		tag.style.display = "";
		img.src = "img/Open.gif";
	} else if(img.src == "img/Open.gif"){
		tag.style.height = "auto";
		img.src = "img/List.gif";
	} else {
		title.style.display = "none";
		tag.style.display = "none";
		tag.style.height = sz;
		img.src = "img/Close.gif";
	}
}
function view_list(tag, img, size){
	if( tag.style.height == "0px" ){
		tag.style.height = size;
		img.src = "img/Open.gif";
	} else if(img.src == "img/Open.gif"){
		tag.style.height = "auto";
		img.src = "img/List.gif";
	} else {
		tag.style.height = 0;
		img.src = "img/Close.gif";
	}
}   

//�J���}�}���֐�
function insComma(str) {
	var num = new String(str).replace(/,/g, "");
	while(num != (num = num.replace(/^(-?\d+)(\d{3})/, "$1,$2")));
	return str;
}
function CinsComma(str) {
	var num = new String(str).replace(/,/g, "");
	while(num != (num = num.replace(/^(-?\d+)(\d{3})/, "$1,$2")));
	return num;
}

//�J���}�폜�֐�
function delComma(w) {
    var z = w.replace(/,/g,"");
    return (z);
}
