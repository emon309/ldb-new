   <?php include_once("navbar_1.php");  ?>

    <div class="side-wrap">
        <nav class="navigation">
            <ul class="nav-main">
                <li>
                    <a class="nav-trigger" href="#"><span>マーケット</span><span class="icon fas fa-chevron-right"></span></a>
                    <ul class="nav-second">
                        <li><a href="search-labo-kenkyusya.html">ラボ・研究者検索</a></li>
                        <li><a href="web-search.html">閲覧履歴検索</a></li>
                        <li><a href="market-search.html">キーワード検索</a></li>
                        <li><a href="search-yosan.html">市場ターゲット検索</a></li>
                        <li><a href="goods-search.html">製品別検索</a></li>
                        <li><a href="jyutyu-search.html">販売店別検索</a></li>
                        <li><a href="#">未紐付き売上出力</a></li>
                        <li><a href="#">修正依頼一覧</a></li>
                    </ul>
                </li>
                <li>
                    <a class="nav-trigger" href="#"><span>業務関連</span><span class="icon fas fa-chevron-right"></span></a>
                    <ul class="nav-second">
                        <li><a href="JavaScript:menu_search_one_hiki('');">引合い登録</a></li>
                        <li><a href="#">受託引合い検索</a></li>
                        <li><a href="#">受託案件検索</a></li>
                        <li><a href="hikiai-search.html">引合い検索</a></li>
                        <li><a href="JavaScript:menu_search_one_bulk('')">バルク案件登録</a></li>
                        <li><a href="bulk-search.html">バルク案件検索</a></li>
                        <li><a href="#">血清案件登録</a></li>
                        <li><a href="#">血清案件検索</a></li>
                        <li><a href="#">受託見積もり検索</a></li>
                        <li><a href="JavaScript:menu_search_one_mitsu('');">試薬・機器見積もり登録</a></li>
                        <li><a href="mitsu-search.html">試薬・機器見積もり検索</a></li>
                        <li><a href="JavaScript:menu_search_one_sample('');">サンプル登録</a></li>
                        <li><a href="sample-search.html">サンプル検索</a></li>
                        <li><a href="#">直送品情報登録</a></li>
                        <li><a href="#">直送品情報検索</a></li>
                        <li><a href="#">機器修理依頼情報管理</a></li>
                        <li><a href="#">機器修理依頼情報検索</a></li>
                        <li><a href="#">デモ機マスタ検索</a></li>
                        <li><a href="#">デモ案件検索</a></li>
                        <li><a href="#">販売店用レポート</a></li>
                    </ul>
                </li>
                <li>
                    <a class="nav-trigger" href="#"><span>TS部業務関連</span><span class="icon fas fa-chevron-right"></span></a>
                    <ul class="nav-second">
                        <li><a href="#">問合わせ・クレーム情報管理</a></li>
                        <li><a href="#">問合わせ・クレーム検索</a></li>
                        <li><a href="#">FAX送信書</a></li>
                        <li><a href="#">FAX送信書検索</a></li>
                        <li><a href="#">無償請求出荷指示</a></li>
                        <li><a href="#">無償請求出荷指示検索</a></li>
                        <li><a href="#">返品連絡書</a></li>
                        <li><a href="#">返品連絡書検索</a></li>
                        <li><a href="#">社内連絡書</a></li>
                        <li><a href="#">社内連絡書検索</a></li>
                        <li><a href="#">商品部FAX送信書</a></li>
                        <li><a href="#">商品部FAX送信書検索</a></li>
                        <li><a href="#">問合わせ依頼書</a></li>
                        <li><a href="#">問合わせ依頼書検索</a></li>
                        <li><a href="#">SDS依頼書</a></li>
                        <li><a href="#">SDS依頼書検索</a></li>
                        <li><a href="#">カタログ発送依頼書</a></li>
                        <li><a href="#">カタログ発送依頼書検索</a></li>
                    </ul>
                </li>
                <li>
                    <a class="nav-trigger" href="#"><span>活動実績</span><span class="icon fas fa-chevron-right"></span></a>
                    <ul class="nav-second">
                        <li><a href="#">コンタクトレコード検索</a></li>
                        <li><a href="#">販売店情報検索</a></li>
                        <li><a href="#">会員情報登録</a></li>
                        <li><a href="#">会員情報検索</a></li>
                        <li><a href="#">会員フラグ情報</a></li>
                    </ul>
                </li>
                <li>
                    <a class="nav-trigger" href="#"><span>発送機能</span><span class="icon fas fa-chevron-right"></span></a>
                    <ul class="nav-second">
                        <li><a href="#">ニュース発送データ一覧</a></li>
                        <li><a href="#">ニュース発送データ登録（ユーザー）</a></li>
                        <li><a href="#">ニュース発送データ登録（販売店）</a></li>
                        <li><a href="#">Daily発送データ一覧</a></li>
                        <li><a href="#">Daily発送データ登録（ユーザー）</a></li>
                        <li><a href="#">Daily発送データ登録（販売店）</a></li>
                        <li><a href="#">Daily発送データ出力</a></li>
                        <li><a href="#">カタログマスタ</a></li>
                        <li><a href="#">送付先販売店マスタ</a></li>
                    </ul>
                </li>
                <li>
                    <a class="nav-trigger" href="#"><span>データインポート</span><span class="icon fas fa-chevron-right"></span></a>
                    <ul class="nav-second">
                        <li><a href="#">社員マスタ</a></li>
                        <li><a href="#">メーカーマスタ</a></li>
                        <li><a href="#">カテゴリマスタ</a></li>
                        <li><a href="#">販売店マスタ</a></li>
                        <li><a href="#">受託見積もりマスタ</a></li>
                        <li><a href="#">機器商品マスタ</a></li>
                        <li><a href="#">デモ機マスタ</a></li>
                        <li><a href="#">マッチリスト</a></li>
                        <li><a href="#">アンマッチリスト</a></li>
                        <li><a href="#">アンマッチリスト２</a></li>
                        <li><a href="#">マッチングアップロード</a></li>
                        <li><a href="#">マッチングアップロード</a></li>
                        <li><a href="#">EDIリスト</a></li>
                    </ul>
                </li>
                <li>
                    <a class="nav-trigger" href="#"><span>環境設定</span><span class="icon fas fa-chevron-right"></span></a>
                    <ul class="nav-second">
                        <li><a href="#">ユーザー管理</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div class="nav-data no-pc">
            <p class="user-data"><?=$name;?>様</p>
            <button class="sign-out btn btn-link" onClick="window.location.href='logout.php'"><i class="fas fa-sign-out-alt"></i>ログアウト</button>
        </div>
    </div>
