<?php
    require_once "database.inc";

    function CV($base, $fmt, $rep){
        $r = "";
        if($base == $fmt){
            $r = $rep;
        } else {
            $r = $base;
        }
        return $r;
    }

    function Selected($base, $ref){
        $r = "";
        if ($base==$ref) {
            $r = "selected";
        } else {
            $r = "";
        }
        return $r;
    }

    function SelectedAry($base, $ary){
        $r = "";

        if (in_array($base, $ary)) {
            $r = "selected";
        } else {
            $r = "";
        }
        return $r;
    }

    function Checked($base, $ref){
        $r = "";
        if ($base==$ref) {
            $r = "checked";
        } else {
            $r = "";
        }
        return $r;
    }

    function CheckedAry($base, $ary){
        $r = "";

        if (in_array($base, $ary)) {
            $r = "checked";
        } else {
            $r = "";
        }
        return $r;
    }

    function AddUnit($str, $pre, $post) {
        $r = "";
        if ($str != "") {
            $r = $pre . $str . $post;
        }
        return $r;
    }

// *********************************************
// DB関連
// *********************************************

    // 単純セレクト
    function DBSelect($sql) {

        $ret = array();

        $conn = Get_Conn();
        pg_query($conn, "set client_encoding=UNICODE");

        $rset = pg_query($conn,$sql);

        if($rset) {
			$c = 0;
            while($array = pg_fetch_assoc($rset)) {

				$array_enc = array(); // 文字コード変換用
                foreach($array as $key => $value) {
                    $array_enc[$key] = $value;

                }
                $ret["Data"][] = $array_enc;
				$c ++;
            }

			if( $c ){
	            $ret["Count"] = count($ret["Data"]);
			} else {
	            $ret["Count"] = 0;
			}
        }

        return $ret;
    }

	function getOne($sql){
		$ret = "";
        $conn = Get_Conn();
        pg_query($conn, "set client_encoding=UNICODE");
        $rset = pg_query($conn,$sql);
        if($rset) {
            if($array = pg_fetch_row($rset)) {
				$ret = $array[0];
			}
		}

		return $ret;
	}

    function getCnt($sql){
        $ret = "";
        $conn = Get_Conn();
        pg_query($conn, "set client_encoding=UNICODE");
        $rset = pg_query($conn,$sql);
        if($rset) {
            $ret = pg_numrows($rset);
        }

        return $ret;
    }

    function MakeSQL($type, $table,$sql_array, $key_sql) {

        $sql = "";

        if ($type=="INSERT") {
            $column_array = array();
            $values_array = array();

            foreach($sql_array as $column => $value) {
                $column_array[] = $column;
                $values_array[] = $value;
            }

            $sql  = "insert into ".$table." (".implode(",",$column_array).") values (".implode(",",$values_array).")";

        } else if ($type=="UPDATE") {
            $sql  = "update ".$table." set ";
            foreach($sql_array as $column => $value) {
                $sql .= $column." = ".$value." ,";
            }
            $sql  = trim($sql,",");
            $sql .= " " . $key_sql;
        }

        return $sql;
    }


    /*==========*/
    /*= ID取得 =*/
    /*==========*/
    function NextID($table) {
        $conn = Get_Conn();

        $sql  = "select nextval('".$table."_seq') as id";

        $rset = pg_query($conn,$sql);

        if($rset) {
            $array = pg_fetch_assoc($rset);

            $id = $array["id"];

            return $id;
        } else {
            return false;
        }
    }

?>
