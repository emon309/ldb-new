<?php
	require_once "database.inc";
	require_once "string.inc";
	require_once "keyword_replace.inc";

	$remote = $_SERVER['REMOTE_ADDR'];

	function Error_Page($message) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis" />
<title>Dealer Order Gateway | ホーム</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body style="text-align:left">
  <div id="header">
   <div class="logo"><img src="/common/img/DOGlogo.jpg" alt="DOG Dealer Order Gateway" border="0" /></div>
  </div>

	<div style="text-align:center;font-size:12px;font-weight:bold;color:#CC0000;">
	<?=$message?>
	<br />
	<br />
	<a href="/DOG/index.php">ホームへ</a>
	</div>
</body>
</html>
<?php
		exit;
	}

	/*=====================================================*/
	/*==                   Entry Point                   ==*/
	/*=====================================================*/

	session_start();

	// ログインのし直しに対応が必要
	if(strcmp(Array_Check($_POST,"status"),"clear") == 0){
		$_SESSION["status"] = "";
	}

	if(strcmp(Array_Check($_SESSION,"status"),"") == 0) {
		// ログインがされていない状態ならば、ログイン処理を行う

		if(strcmp(Array_Check($_POST,"mode"),"login") == 0){
			// ログインページからサブミットされた場合
			$staff_no = Array_Check($_POST,"staff_no");
			$password = Array_Check($_POST,"password");

			$sess_conn  = Get_Conn();

			$sess_rset  = pg_query($sess_conn,"select * from mb_user where user_code = '".pg_escape_string($staff_no)."' and pass = '".pg_escape_string($password)."' and del_flg='1'");

			if($sess_rset && pg_num_rows($sess_rset) == 1) {
				$sess_array = pg_fetch_assoc($sess_rset);
				$_SESSION["staff_no"] = Array_Check($sess_array,"user_code");
				$_SESSION["staff_name"] = Array_Check($sess_array,"user_name1").Array_Check($sess_array,"user_name2");
				$_SESSION["status"]     = "1";
				$_SESSION["roll"] = Array_Check($sess_array,"roll");
				$_SESSION["company_no"] = Array_Check($sess_array,"company_no");
				$_SESSION["company_name"] = Array_Check($sess_array,"company_name");
				$_SESSION["dairi"] = Array_Check($sess_array,"dairi");
				$_SESSION["maker"] = Array_Check($sess_array,"maker");

				$sess_rset  = pg_query($sess_conn,"select * from mb_company where company_no = '".pg_escape_string($_SESSION["company_no"])."' and del_flg='0' order by company_kyoten_code limit 1");
				if($sess_rset && pg_num_rows($sess_rset) == 1) {
					$sess_array = pg_fetch_assoc($sess_rset);
					$_SESSION["company_name"] = Array_Check($sess_array,"company_name");
					$_SESSION["dairi"] = Array_Check($sess_array,"dairi");
					$_SESSION["maker"] = Array_Check($sess_array,"maker");
				} else {
					$sess_rset  = pg_query($sess_conn,"select * from mb_use_company where company_no = '".pg_escape_string($_SESSION["company_no"])."' and del_flg='0' order by company_kyoten_code limit 1");
					if($sess_rset && pg_num_rows($sess_rset) == 1) {
						$sess_array = pg_fetch_assoc($sess_rset);
						$_SESSION["company_name"] = Array_Check($sess_array,"company_name");
						$_SESSION["dairi"] = "1";
					} else {
						Error_Page("法人情報が取得できません。");
						retrun;
					}

					$sess_rset  = pg_query($sess_conn,"select * from mb_conv_comp where company_code = '".pg_escape_string($_SESSION["company_no"])."' and company_kyoten_code = '".pg_escape_string($staff_no)."' and del_flg='0' limit 1");
					if($sess_rset && pg_num_rows($sess_rset) == 1) {
						$sess_array = pg_fetch_assoc($sess_rset);
						$_SESSION["tokui_code"] = Array_Check($sess_array,"tokui_code");
					} else {
						Error_Page($sql."法人情報が取得できません。");
						retrun;
					}
				}
			} else {
				// スタッフ番号とパスワードが一致しなければ、エラーページを表示して終了
				Error_Page("ユーザーIDとパスワードが一致しませんでした。");
			}
		} else {
			// それ以外の場合(ログインせずに直接ページへアクセスしてきた場合)
			Error_Page("ログインページからログインをしてください。");
		}
	} else if(strcmp(Array_Check($_SESSION,"status"),"1") == 0) {
		// ログイン中
	} else {
		// ステータスが不明な場合
		Error_Page("ログインページからログインをしてください。");
	}
?>
