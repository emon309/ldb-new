<?php
	require_once "database.inc";
	require_once "string.inc";
	require_once "keyword_replace.inc";

	$remote = $_SERVER['REMOTE_ADDR'];

	function Error_Page($message) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis" />
<title>ラボラトリ・データベース | ログインエラー</title>
<link href="/common/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="container">
  <div id="header"><div class="logo"><img src="/common/img/logo.gif" alt="LDB ラボラトリ・データベース" width="274" height="60" border="0" /></div>
  </div>
 <div id="main">
	<div style="text-align:center;font-size:12px;font-weight:bold;color:#CC0000;">
	<?=$message?>
	<br />
	<br />
	<a href="/index.html">ホームへ</a>
	</div>
 </div>
 <div id="footer"><span class="footertext">
<img src="/common/img/arrow03.gif" width="9" height="9" /> <a href="securitypolicy.html">セキュリティポリシー</a>　<img src="/common/img/arrow03.gif" width="9" height="9" /> <a href="http://www.sag-brain.jp/" target="_blank">運営会社</a></span> Copyrightc 2007-2008 SAGBRAIN Corp . All rights reserved. 　　　　　　　　　　　　<a href="http://www.sag-brain.jp/" target="_blank"><img src="/common/img/footer_logo.gif" alt="SAGBRAIN" width="143" height="40" border="0" align="absmiddle" /></a></div>
</div>
</body>
</html>
<?php
		exit;
	}

	/*=====================================================*/
	/*==                   Entry Point                   ==*/
	/*=====================================================*/

	session_start();

	// ログインのし直しに対応が必要
	if(strcmp(Array_Check($_POST,"status"),"clear") == 0){
		$_SESSION["status"] = "";
	}

	if(strcmp(Array_Check($_SESSION,"status"),"") == 0) {
		// ログインがされていない状態ならば、ログイン処理を行う

		if(Array_Check($_POST,"mode") == "login"){
			// ログインページからサブミットされた場合
			$staff_no = Array_Check($_POST,"staff_no");
			$password = Array_Check($_POST,"password");

			$sess_conn  = Get_Conn();

			$sess_rset  = pg_query($sess_conn,"select to_char(mt_staff.utime + '14 Days', 'yyyy年mm月dd日') as password_limit, mt_staff.*, mt_company.company_id, mt_company.photo, mt_company.user_max, mt_company.select_area from mt_staff, mt_company where mt_company.company_no = mt_staff.company_no and mt_staff.del_flg = '0' and mt_staff.staff_no = '".pg_escape_string($staff_no)."' and mt_staff.password = '".pg_escape_string($password)."'");


			if($sess_rset && pg_num_rows($sess_rset) == 1) {
				$sess_array = pg_fetch_assoc($sess_rset);

				// スタッフ番号とパスワードが一致したならば、スタッフ番号とステータスをセット
				$_SESSION["staff_no"] = Array_Check($sess_array,"staff_no");
				$_SESSION["staff_name"] = Array_Check($sess_array,"name");
				$_SESSION["status"]     = "1";
				$_SESSION["company_no"] = Array_Check($sess_array,"company_no");
				$_SESSION["company_id"] = Array_Check($sess_array,"company_id");
				$_SESSION["kengen"] = Array_Check($sess_array,"kengen");
				$_SESSION["password_kigen"] = Array_Check($sess_array,"password_kigen");
				$_SESSION["password_limit"] = Array_Check($sess_array,"password_limit");
				$_SESSION["logo"] = Array_Check($sess_array,"photo");
				$_SESSION["user_max"] = Array_Check($sess_array,"user_max");
				$_SESSION["select_area"] = Array_Check($sess_array,"select_area");
				pg_query($sess_conn, "update mt_staff set last_login = now() where staff_no = '".pg_escape_string($staff_no)."'");
				if( $_SESSION["company_no"] == "FN" ){
					$rs = pg_query($sess_conn, "select * from fn_member where member_code = trim('".pg_escape_string($staff_no)."')");
					if($rs && pg_num_rows($rs) == 1) {
						$ar = pg_fetch_assoc($rs);
						$_SESSION["member_code"] = Array_Check($ar,"member_code");
						$_SESSION["member_name"] = Array_Check($ar,"member_name");
						$_SESSION["member_group"] = Array_Check($ar,"member_group");
						$_SESSION["member_pass"] = Array_Check($ar,"member_pass");
						$_SESSION["member_roll"] = Array_Check($ar,"member_roll");
						$_SESSION["member_roll1"] = Array_Check($ar,"member_roll1");
						$_SESSION["member_roll2"] = Array_Check($ar,"member_roll2");
						$_SESSION["member_roll3"] = Array_Check($ar,"member_roll3");
						$_SESSION["member_roll4"] = Array_Check($ar,"member_roll4");
						$_SESSION["member_roll5"] = Array_Check($ar,"member_roll5");
						$_SESSION["member_roll6"] = Array_Check($ar,"member_roll6");
						$_SESSION["member_roll7"] = Array_Check($ar,"member_roll7");
						$_SESSION["member_roll8"] = Array_Check($ar,"member_roll8");
						$_SESSION["member_roll9"] = Array_Check($ar,"member_roll9");
					}
				}

				$sql = "select login_cnt, sum_cnt, to_char(regdate, 'yyyy/mm/dd') as regdate, to_char(now(), 'yyyy/mm/dd') as nowdate from staff_log where staff_no = '".pg_escape_string($staff_no)."'";
				$rset = pg_query($sess_conn,$sql);
				if($rset && pg_num_rows($rset)) {
					$sess_array = pg_fetch_assoc($rset);
					$login_cnt = Array_Check($sess_array,"login_cnt");
					$sum_cnt = Array_Check($sess_array,"sum_cnt");
					$regdate = Array_Check($sess_array,"regdate");
					$nowdate = Array_Check($sess_array,"nowdate");
					if( $regdate != $nowdate ){
						$login_cnt ++;
					}
					$sum_cnt ++;
					$sql = "update staff_log set login_cnt = '${login_cnt}', sum_cnt = '${sum_cnt}', regdate = now() where staff_no = '".pg_escape_string($staff_no)."'";
					pg_query($sess_conn,$sql);
				} else {
					$login_cnt = 1;
					$sum_cnt = 1;
					pg_query($sess_conn,"insert into staff_log (staff_no, login_cnt, sum_cnt, regdate) values('".pg_escape_string($staff_no)."', '${login_cnt}', '${sum_cnt}', now())");
				}
			} else {
				// スタッフ番号とパスワードが一致しなければ、エラーページを表示して終了
				Error_Page("ユーザーIDとパスワードが一致しませんでした。");
			}
		} else {
			// それ以外の場合(ログインせずに直接ページへアクセスしてきた場合)
			Error_Page("ログインページからログインをしてください。");
		}
	} else if(strcmp(Array_Check($_SESSION,"status"),"1") == 0) {
		// ログイン中
	} else {
		// ステータスが不明な場合
		Error_Page("ログインページからログインをしてください。");
	}
?>
