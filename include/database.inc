<?php
	

//modyfied by inhouse team member
function Get_Conn() {
		$database_host     = "localhost";
		$database_port     = 5432;
		$database_name     = "ldbfn";
		$database_user     = "postgres";
		$database_password = "ldbfn1";

		$connection = pg_connect("host=".$database_host." port=".$database_port." dbname=".$database_name." user=".$database_user." password=".$database_password);

		if(!$connection){
        $errormessage=pg_last_error();
	echo "Error 0: " . $errormessage;
	echo "couldn't connect";
        exit();
		}else{
			//echo "connected";
      pg_set_client_encoding( $connection, "SJIS" );

    }
		

		return $connection;
	}

	function Create_INSERT_SQL($table,$sql_array) {
		$column_array = array();
		$values_array = array();

		foreach($sql_array as $column => $value) {
			$column_array[] = $column;
			$values_array[] = $value;
		}

		$sql  = "insert into ".$table." (".implode(",",$column_array).") values (".implode(",",$values_array).")";

		return $sql;
	}

	function Create_UPDATE_SQL($table,$sql_array) {
		$sql  = "update ".$table." set ";
		foreach($sql_array as $column => $value) {
			$sql .= $column." = ".$value." ,";
		}
		$sql  = trim($sql,",");

		return $sql;
	}

	/*==========*/
	/*= ID取得 =*/
	/*==========*/
	function Next_ID($table) {
		$conn = Get_Conn();

		$sql  = "select nextval('".$table."_seq') as id";

		$rset = pg_query($conn,$sql);

		if($rset) {
			$array = pg_fetch_assoc($rset);

			$id = $array["id"];

			return $id;
		} else {
			return false;
		}
	}

	/*========================*/
	/*= JF職員コードチェック =*/
	/*========================*/
	function JF_Staff_Code_Check($jf_staff_code,$jf_staff_id="") {
		if(strcmp($jf_staff_code,"") == 0) {
			return "職員コードが入力されていません";
		} else {
			$conn = Get_Conn();

			$sql  = "select count(*) as count from jf_staff where jf_staff_code = '".pg_escape_string($jf_staff_code)."'";
			if(strcmp($jf_staff_id,"") != 0){
				// IDが存在する場合は、IDのデータを除外する(UPDATEなどで使用)
				$sql .= " and jf_staff_id != ".pg_escape_string($jf_staff_id);
			}

			$rset = pg_query($conn,$sql);

			if($rset) {
				$array = pg_fetch_assoc($rset);

				$count = Array_Check($array,"count");

				if(strcmp($count,"0") == 0) {
					return "";
				} else {
					return "既に使用されている職員コードです";
				}
			}
		}
	}

	/*========================*/
	/*= 試験名称重複チェック =*/
	/*========================*/
	function Exam_Name_Check($exam_name,$exam_id="") {
		if(strcmp($exam_name,"") == 0) {
			return "試験名称が入力されていません";
		} else {
			$conn = Get_Conn();

			$sql  = "select count(*) as count from exam";
			$sql .= " where exam_name = '".pg_escape_string($exam_name)."'";
			if(strcmp($exam_id,"") != 0) {
			$sql .= "   and exam_id != ".pg_escape_string($exam_id);
			}

			$rset = pg_query($conn,$sql);

			if($rset) {
				$array = pg_fetch_assoc($rset);

				if(strcmp($array["count"],"0") == 0) {
					return "";
				} else {
					return "入力された試験名称は、すでに使用されています";
				}
			} else {
				return "試験名称の確認に失敗しました";
			}
		}
	}

	/*============*/
	/* 試験名取得 */
	/*============*/
	function Exam_Name($exam_id) {
		if(strcmp($exam_id,"") == 0) {
			return "";
		}

		$exam_name = "";

		$conn = Get_Conn();

		$sql  = "select exam_name from exam where exam_id = '".pg_escape_string($exam_id)."'";

		$rset = pg_query($conn,$sql);

		if($rset && pg_num_rows($rset) == 1) {
			$array = pg_fetch_assoc($rset);

			$exam_name = $array["exam_name"];
		}

		return $exam_name;
	}

	/*==============*/
	/* エリア名取得 */
	/*==============*/
	function Area_Name($area_id) {
		if(strcmp($area_id,"") == 0) {
			return "";
		}

		$area_name = "";

		$conn = Get_Conn();

		$sql  = "select area_name from area where area_id = '".pg_escape_string($area_id)."'";

		$rset = pg_query($conn,$sql);

		if($rset && pg_num_rows($rset) == 1) {
			$array = pg_fetch_assoc($rset);

			$area_name = $array["area_name"];
		}

		return $area_name;
	}

	/*==================*/
	/* エリア詳細名取得 */
	/*==================*/
	function Detail_Area_Name($detail_area_id) {
		if(strcmp($detail_area_id,"") == 0) {
			return "";
		}

		$detail_area_name = "";

		$conn = Get_Conn();

		$sql  = "select detail_area_name from detail_area where detail_area_id = '".pg_escape_string($detail_area_id)."'";

		$rset = pg_query($conn,$sql);

		if($rset && pg_num_rows($rset) == 1) {
			$array = pg_fetch_assoc($rset);

			$detail_area_name = $array["detail_area_name"];
		}

		return $detail_area_name;
	}

	/*==========*/
	/* 国名取得 */
	/*==========*/
	function Country_Name($country_id) {
		if(strcmp($country_id,"") == 0) {
			return "";
		}

		$country_name = "";

		$conn = Get_Conn();

		$sql  = "select country_name from country where country_id = ".pg_escape_string($country_id);

		$rset = pg_query($conn,$sql);

		if($rset && pg_num_rows($rset) == 1) {
			$array = pg_fetch_assoc($rset);

			$country_name = $array["country_name"];
		}

		return $country_name;
	}

	/*============*/
	/* 都市名取得 */
	/*============*/
	function City_Name($city_id) {
		if(strcmp($city_id,"") == 0) {
			return "";
		}

		$city_name = "";

		$conn = Get_Conn();

		$sql  = "select city_name from city where city_id = ".pg_escape_string($city_id);

		$rset = pg_query($conn,$sql);

		if($rset && pg_num_rows($rset) == 1) {
			$array = pg_fetch_assoc($rset);

			$city_name = $array["city_name"];
		}

		return $city_name;
	}

	/*============*/
	/* 会場名取得 */
	/*============*/
	function Kaijyo_Name($kaijyo_id) {
		if(strcmp($kaijyo_id,"") == 0) {
			return "";
		}

		$kaijyo_name = "";

		$conn = Get_Conn();

		$sql  = "select kaijyo_name from kaijyo where kaijyo_id = ".pg_escape_string($kaijyo_id);

		$rset = pg_query($conn,$sql);

		if($rset && pg_num_rows($rset) == 1) {
			$array = pg_fetch_assoc($rset);

			$kaijyo_name = $array["kaijyo_name"];
		}

		return $kaijyo_name;
	}

	/*============*/
	/* 組織名取得 */
	/*============*/
	function Office_Name($office_id) {
		if(strcmp($office_id,"") == 0) {
			return "";
		}

		$office_name = "";

		$conn = Get_Conn();

		$sql  = "select office_name from office where office_id = ".pg_escape_string($office_id);

		$rset = pg_query($conn,$sql);

		if($rset && pg_num_rows($rset) == 1) {
			$array = pg_fetch_assoc($rset);

			$office_name = $array["office_name"];
		}

		return $office_name;
	}

	/*============*/
	/* 業務名取得 */
	/*============*/
	function Business_Name($business_id) {
		if(strcmp($business_id,"") == 0) {
			return "";
		}

		$business_name = "";

		$conn = Get_Conn();

		$sql  = "select business_name from business where business_id = ".pg_escape_string($business_id);

		$rset = pg_query($conn,$sql);

		if($rset && pg_num_rows($rset) == 1) {
			$array = pg_fetch_assoc($rset);

			$business_name = $array["business_name"];
		}

		return $business_name;
	}
?>
