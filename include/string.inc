<?php
	define("SPACE_IS_NO_BLANK",0); // Blunk_Checkにて使用
	define("SPACE_IS_BLANK"   ,1); // Blunk_Checkにて使用

	function mb_trim($str,$charlist=" ") {
		$str = mb_ereg_replace("^[".$charlist."]+","",$str);
		$str = mb_ereg_replace("[".$charlist."]+$","",$str);

		return $str;
	}

	function hs($str,$option=ENT_QUOTES) {
		return htmlspecialchars($str,$option);
	}

	/*
		配列チェック

		第1引数($array)   ：元となる配列を渡す
		第2引数($key)     ：チェックするキーを渡す
		第3引数($null_str)：に配列中にキーが存在しない、もしくは空(nullを含む)の場合に返す文字列を指定する(デフォルト="")

		戻り値：キーがセットされていたらその値($array[$key])を返す。キーがセットされていない、または空(nullを含む)だったら第3引数($null_str)を返す。
		        また、第1引数($array)が配列でない場合も第3引数($null_str)を返す。
	*/
	function Array_Check($array,$key,$null_str="") {
		if(is_array($array)) {
			if(array_key_exists($key,$array)) {
				if(strcmp($array[$key],"") == 0) {
					return $null_str;
				} else if($array[$key] == null) {
					return $null_str;
				} else {
					return $array[$key];
				}
			} else {
				return $null_str;
			}
		} else {
			return $null_str;
		}
	}
	function Array_Check_utf2sjis($array,$key,$null_str="") {
		if(is_array($array)) {
			if(array_key_exists($key,$array)) {
				if(strcmp($array[$key],"") == 0) {
					return $null_str;
				} else if($array[$key] == null) {
					return $null_str;
				} else {
					return mb_convert_encoding($array[$key], 'sjis-win', 'UTF-8');
				}
			} else {
				return $null_str;
			}
		} else {
			return $null_str;
		}
	}

	/*
		セレクトタグ用文字列比較

		第1引数($value1)：比較する文字列を渡す
		第2引数($value2)：比較する文字列を渡す
		第3引数($return)：2つの文字列が同一だった場合に返す値を指定する(デフォルト=null)

		戻り値：2つの文字列が同一ならば第3引数($return)(nullの場合は" selected ")を返し、そうでないならば空を返す。
	*/
	function Select_Check($value1,$value2,$return=null) {
		if(strcmp($value1,$value2) == 0) {
			if(is_null($return)) {
				return " selected ";
			} else {
				return $return;
			}
		} else {
			return "";
		}
	}

	/*
		空文字チェック

		第1引数($value) ：チェックする文字列を渡す
		第2引数($return)：戻り値として返す値を渡す
		第3引数($space) ：スペースとタブに対する扱いを指定するフラグを渡す(デフォルト=0(SPACE_IS_NO_BLANK))
		                ："0"(SPACE_IS_NO_BLANK)を指定した場合、スペース(全半角)とタブのみの文字列を空文字として扱わない
		                ："1"(SPACE_IS_BLANK)を指定した場合、スペース(全半角)とタブのみの文字列を空文字として扱う

		戻り値：第1引数($value)が空もしくはnullの場合、第2引数($return)を返す。そうでないならば第1引数($value)を返す。
	*/
	function Blank_Check($value,$return,$space=SPACE_IS_NO_BLANK) {
		// SPACE_IS_BLANKが指定された場合、スペースとタブのみの文字列を空にする
		if($space == SPACE_IS_BLANK && preg_match("/^(( )|(　)|(\t))+$/",$value) == 1) {
			$value = "";
		}

		if(is_null($value) || strcmp($value,"") == 0) {
			return $return;
		} else {
			return $value;
		}
	}

	/*
		エラーログ出力

		第1引数($error_msg)：エラーメッセージを渡す

		渡された文字列に、日時とスクリプトファイル名を付加してファイルに出力する
		もし、定数"ERROR_LOG_FILE"が未定義の場合は、"/tmp/"以下にユーザー名(スクリプト実行者)を付加したファイル名で出力する。
	*/
	function Out_Error_Log($error_msg) {
		$log_file = defined("ERROR_LOG_FILE") ? ERROR_LOG_FILE : "/tmp/".Array_Check($_SERVER,"USER")."_error.log" ;

		error_log("#==[".date("Y/m/d G:i:s")."] [".basename($_SERVER['SCRIPT_FILENAME'])."]=========================================================#\n\n".$error_msg."\n\n",3,$log_file);
	}
?>
