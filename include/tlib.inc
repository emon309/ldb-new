<?php
    require_once "database.inc";

    function CV($base, $fmt, $rep){
        $r = "";
        if($base == $fmt){
            $r = $rep;
        } else {
            $r = $base;
        }
        return $r;
    }

    function Selected($base, $ref){
        $r = "";
        if ($base==$ref) {
            $r = "selected";
        } else {
            $r = "";
        }
        return $r;
    }

    function SelectedAry($base, $ary){
        $r = "";

        if (in_array($base, $ary)) {
            $r = "selected";
        } else {
            $r = "";
        }
        return $r;
    }

    function Checked($base, $ref){
        $r = "";
        if ($base==$ref) {
            $r = "checked";
        } else {
            $r = "";
        }
        return $r;
    }

    function CheckedAry($base, $ary){
        $r = "";

        if (in_array($base, $ary)) {
            $r = "checked";
        } else {
            $r = "";
        }
        return $r;
    }

    function AddUnit($str, $pre, $post) {
        $r = "";
        if ($str != "") {
            $r = $pre . $str . $post;
        }
        return $r;
    }


    function COStaffArea($str) {
        $r = "";

        $ken = array();
$ken[] = "足立区";
$ken[] = "荒川区";
$ken[] = "板橋区";
$ken[] = "江戸川区";
$ken[] = "大田区";
$ken[] = "葛飾区";
$ken[] = "北区";
$ken[] = "江東区";
$ken[] = "品川区";
$ken[] = "渋谷区";
$ken[] = "新宿区";
$ken[] = "杉並区";
$ken[] = "墨田区";
$ken[] = "世田谷区";
$ken[] = "台東区";
$ken[] = "千代田区";
$ken[] = "中央区";
$ken[] = "豊島区";
$ken[] = "中野区";
$ken[] = "練馬区";
$ken[] = "文京区";
$ken[] = "港区";
$ken[] = "目黒区";
$ken[] = "東京都下";
$ken[] = "埼玉県";
$ken[] = "群馬県";
$ken[] = "栃木県";
$ken[] = "新潟県";
$ken[] = "東京大学　本郷地区";
$ken[] = "東京大学　弥生地区";
$ken[] = "理化学研究所";
$ken[] = "国立国際医療ｾﾝﾀｰ";
$ken[] = "感染研　戸山";
$ken[] = "東大病院";

        for ($i=0; $i<count($ken); $i++) {
            $r .= "<option value='".$ken[$i]."' ".Selected($str, $ken[$i]).">".$ken[$i]."</option>\n";

        }


        return $r;



    }


	function COMaker($nm) {
		$option_str = "";

		$conn = Get_Conn();
		pg_query($conn, "set client_encoding=UNICODE");

		$sql  = "select col5";
		$sql .= "  from tomy_syohin ";
		$sql .= " group by col5";
		$sql .= " order by col5";
		$rset = pg_query($conn,$sql);

		if($rset) {
			$option_str .= "<option value=''>選択してください</option>";
			while($array = pg_fetch_assoc($rset)) {
				$col5 = mb_convert_encoding(Array_Check($array, "col5"), 'sjis-win', 'UTF-8');
				$option_str .= "<option value='".$col5."' ".Selected($col5, $nm).">".htmlspecialchars($col5,ENT_QUOTES)."</option>";
			}

		}
		return $option_str;
	}

	function COBunruiSyozai($nm, $bn) {
		$option_str = "<option value=''>選択してください</option>";

		$conn = Get_Conn();
		pg_query($conn, "set client_encoding=UNICODE");

		$sql  = "select col4";
		$sql .= "  from tomy_seihin ";
		$sql .= " where col4 <> '' ";
		if( $nm ){
		}
		$sql .= " group by col4 ";
		$sql .= " order by col4 ";
		$rset = pg_query($conn,$sql);

		if($rset) {
			while($array = pg_fetch_assoc($rset)) {
				$col4 = mb_convert_encoding(Array_Check($array, "col4"), 'sjis-win', 'UTF-8');
				$option_str .= "<option value='".$col4."' ".Selected($col4, $bn).">".htmlspecialchars($col4,ENT_QUOTES)."</option>";
			}

		}
		return $option_str;
	}

	function COBunrui($nm, $bn) {
		$option_str = "<option value=''>選択してください</option>";
/*
		if( ! $nm ){
			return $option_str;
		}
*/

		$conn = Get_Conn();
		pg_query($conn, "set client_encoding=UNICODE");

		$sql  = "select col6";
		$sql .= "  from tomy_syohin ";
		$sql .= " where col6 <> '' ";
		if( $nm ){
			$sql .= " and col5 = '".mb_convert_encoding($nm, 'UTF-8', 'sjis-win')."'";
		}
		$sql .= " group by col6 ";
		$sql .= " order by col6 ";
		$rset = pg_query($conn,$sql);

		if($rset) {
			while($array = pg_fetch_assoc($rset)) {
				$col6 = mb_convert_encoding(Array_Check($array, "col6"), 'sjis-win', 'UTF-8');
				$option_str .= "<option value='".$col6."' ".Selected($col6, $bn).">".htmlspecialchars($col6,ENT_QUOTES)."</option>";
			}

		}
		return $option_str;
	}

	function COKatashikiSyozai($nm, $bn, $kt) {
		$option_str = "<option value=''>選択してください</option>";
		if( ! $bn){
			return $option_str;
		}

		$conn = Get_Conn();
		pg_query($conn, "set client_encoding=UNICODE");

		$sql  = "select col6";
		$sql .= "  from tomy_seihin ";
		$sql .= " where col6 <> '' ";
		if( $nm ){
		}
		if( $bn ){
			$sql .= " and col4 = '".mb_convert_encoding($bn, 'UTF-8', 'sjis-win')."'";
		}
		$sql .= " group by col6 ";
		$sql .= " order by col6 ";
		
		$rset = pg_query($conn,$sql);

		if($rset) {
			while($array = pg_fetch_assoc($rset)) {
				$col6 = mb_convert_encoding(Array_Check($array, "col6"), 'sjis-win', 'UTF-8');
				$option_str .= "<option value='".$col6."' ".Selected($col6, $kt).">".htmlspecialchars($col6,ENT_QUOTES)."</option>";
			}

		}
		return $option_str;
	}

	function COKatashiki($nm, $bn, $kt) {
		$option_str = "<option value=''>選択してください</option>";
		if( ! $bn){
			return $option_str;
		}

		$conn = Get_Conn();
		pg_query($conn, "set client_encoding=UNICODE");

		$sql  = "select col7";
		$sql .= "  from tomy_syohin ";
		$sql .= " where col7 <> '' ";
		if( $nm ){
			$sql .= " and col5 = '".mb_convert_encoding($nm, 'UTF-8', 'sjis-win')."'";
		}
		if( $bn ){
			$sql .= " and col6 = '".mb_convert_encoding($bn, 'UTF-8', 'sjis-win')."'";
		}
		$sql .= " group by col7 ";
		$sql .= " order by col7 ";
		$rset = pg_query($conn,$sql);

		if($rset) {
			while($array = pg_fetch_assoc($rset)) {
				$col7 = mb_convert_encoding(Array_Check($array, "col7"), 'sjis-win', 'UTF-8');
				$option_str .= "<option value='".$col7."' ".Selected($col7, $kt).">".htmlspecialchars($col7,ENT_QUOTES)."</option>";
			}

		}
		return $option_str;
	}

// *********************************************
// DB関連
// *********************************************

    // 単純セレクト
    function DBSelect($sql) {

        $ret = array();

$sql = mb_convert_encoding($sql, 'UTF-8', 'sjis-win');

        $conn = Get_Conn();
        pg_query($conn, "set client_encoding=UNICODE");

        $rset = pg_query($conn,$sql);

        if($rset) {
			$c = 0;
            while($array = pg_fetch_assoc($rset)) {

				$array_enc = array(); // 文字コード変換用
                foreach($array as $key => $value) {
                    $array_enc[$key] = mb_convert_encoding($value, 'sjis-win', 'UTF-8');

                }
                $ret["Data"][] = $array_enc;
				$c ++;
            }

			if( $c ){
	            $ret["Count"] = count($ret["Data"]);
			} else {
	            $ret["Count"] = 0;
			}
        }

        pg_query($conn, "set client_encoding=SJIS");
        return $ret;
    }

	function getOne($sql){
		$ret = "";
        $conn = Get_Conn();
        $rset = pg_query($conn,$sql);
        if($rset) {
            if($array = pg_fetch_row($rset)) {
				$ret = $array[0];
			}
		}

		return $ret;
	}

    function getCnt($sql){
        $ret = "";
        $conn = Get_Conn();
        $rset = pg_query($conn,$sql);
        if($rset) {
            $ret = pg_numrows($rset);
        }

        return $ret;
    }

    function MakeSQL($type, $table,$sql_array, $key_sql) {

        $sql = "";

        if ($type=="INSERT") {
            $column_array = array();
            $values_array = array();

            foreach($sql_array as $column => $value) {
                $column_array[] = $column;
                $values_array[] = $value;
            }

            $sql  = "insert into ".$table." (".implode(",",$column_array).") values (".implode(",",$values_array).")";

        } else if ($type=="UPDATE") {
            $sql  = "update ".$table." set ";
            foreach($sql_array as $column => $value) {
                $sql .= $column." = ".$value." ,";
            }
            $sql  = trim($sql,",");
            $sql .= " " . $key_sql;
        }

        return $sql;
    }


    /*==========*/
    /*= ID取得 =*/
    /*==========*/
    function NextID($table) {
        $conn = Get_Conn();

        $sql  = "select nextval('".$table."_seq') as id";

        $rset = pg_query($conn,$sql);

        if($rset) {
            $array = pg_fetch_assoc($rset);

            $id = $array["id"];

            return $id;
        } else {
            return false;
        }
    }





// *********************************************
// セレクトボックス
// *********************************************
    // セレクト部品生成
    function CreateOptionGoodsBunrui($sel="") {
        $option_str = "";

        $conn = Get_Conn();
        pg_query($conn, "set client_encoding=UNICODE");

        $sql  = "select col1";
        $sql .= "  from tomy_syoujyo ";
        $sql .= " group by col1";
        $sql .= " order by col1";
        $rset = pg_query($conn,$sql);

        if($rset) {
            while($array = pg_fetch_assoc($rset)) {
                $name = mb_convert_encoding(Array_Check($array, "col1"), 'sjis-win', 'UTF-8');
                $chk = "";
                if( $sel == $name ){
                    $chk = "selected";
                }
                $option_str .= "<option ".$chk." value='".$name."' ".">".htmlspecialchars($name,ENT_QUOTES)."</option>\n";
            }
        }

        pg_query($conn, "set client_encoding=SJIS");
        return $option_str;
    }

    function CreateOptionGoodsName($sel, $sel2) {

        $option_str = "";

if ($sel!="" && $sel2!="") {


        $conn = Get_Conn();
        pg_query($conn, "set client_encoding=UNICODE");

        $sql  = "select col2";
        $sql .= "  from tomy_syoujyo ";
        $sql .= " where col1='".mb_convert_encoding($sel, 'UTF-8', 'sjis-win')."'";
        $sql .= " group by col2";
        $sql .= " order by col2";
        $rset = pg_query($conn,$sql);

        if($rset) {
            while($array = pg_fetch_assoc($rset)) {
                $name = mb_convert_encoding(Array_Check($array, "col2"), 'sjis-win', 'UTF-8');
                $chk = "";
                if( $sel2 == $name ){
                    $chk = "selected";
                }
                $option_str .= "<option ".$chk." value='".$name."' ".">".htmlspecialchars($name,ENT_QUOTES)."</option>\n";
            }
        }

        pg_query($conn, "set client_encoding=SJIS");
}

        return $option_str;
    }



?>
